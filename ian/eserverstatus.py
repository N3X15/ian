from enum import IntEnum


class EServerStatus(IntEnum):
    STOPPED    = 0
    STARTING   = 1
    COMPILING  = 2
    STARTED    = 3
    LOCKED_OUT = 4
    STOPPING   = 5