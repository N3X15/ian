'''
ian.d Server Monitoring Daemon
'''

def main():
    import argparse

    mon = ServerMonitoringLoop()
    mon.start()