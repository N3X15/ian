import gettext

import pkg_resources

locale_path: str = pkg_resources.resource_filename('ian', 'locales')

def getDomainTxn(domain: str) -> gettext.NullTranslations:
    return gettext.translation(domain=domain, localedir=locale_path, fallback=True).gettext
