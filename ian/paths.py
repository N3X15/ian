from pathlib import Path
import sys

from ian.consts import IS_COMPILED


SCRIPT_PATH: Path = None

if IS_COMPILED:
    SCRIPT_PATH = Path(sys.argv[0])
else:
    SCRIPT_PATH = Path(__file__)
HOME_DIR: Path = Path.home()
IAN_DIR: Path = HOME_DIR / '.ian'
CONFIG_FILE: Path = IAN_DIR / 'config.yml'  
