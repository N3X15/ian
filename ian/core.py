import logging
from typing import Dict

from ian.daemoninstance import DaemonInstance

from ruamel.yaml import YAML as Yaml
from ian.paths import CONFIG_FILE
from i18n import getDomainTxn

_ = getDomainTxn

YAML = Yaml(typ='rt')

log = logging.getLogger(__name__)

class Core:
    def __init__(self) -> None:
        self.instances: Dict[str, DaemonInstance] = {}
        
    def load(self) -> None:
        if not CONFIG_FILE.is_file():
            log.critical(_('Config file {FILENAME} does not exist. Please run `ian init`!').format(FILENAME=str(CONFIG_FILE)))
        with CONFIG_FILE.open('r') as f:
            YAML.load(f)