from ipaddress import ip_address
from typing import Any, Dict

from ian.ipaddr import LOOPBACK, AnyIPAddress


class MonitorConfig:
    def __init__(self) -> None:
        self.ip: AnyIPAddress = LOOPBACK
        self.port: int = 7777
        self.timeout: float = 30.
        self.maxfails: int = 3
        self.waitforready: bool = True
        self.threads: bool = False

    def serialize(self) -> Dict[str, Any]:
        return {
            'ip': str(self.ip),
            'port': self.port,
            'timeout': self.timeout,
            'max-fails': self.maxfails,
            'wait-for-ready': self.waitforready,
            'threads': self.threads,
        }

    def deserialize(self, data: Dict[str, Any]) -> None:
        self.ip = ip_address(data['ip'])
        self.port = int(data['port'])
        self.timeout = float(data['timeout'])
        self.maxfails = int(data['max-fails'])
        self.waitforready = bool(data['waitforready'])
        self.threads = bool(data['threads'])

    def set_defaults(self) -> None:
        self.ip = LOOPBACK
        self.port = 7777
        self.timeout = 30.
        self.maxfails = 3
        self.waitforready = True
        self.threads = False
