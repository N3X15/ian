from typing import Any, Dict


class BaseFlavorConfig:
    def __init__(self) -> None:
        self.id: str = ''

    def serialize(self) -> Dict[str, Any]:
        return {'id': self.id}

    def deserialize(self, data: Dict[str, Any]) -> None:
        self.id = data['id']