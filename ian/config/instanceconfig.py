from ipaddress import IPv4Address, ip_address
from typing import Any, Dict

from ian.config.monitorconfig import MonitorConfig
from ian.ipaddr import AnyIPAddress


class InstanceConfig:
    def __init__(self) -> None:
        self.id: str = ''
        self.notes: str = ''
        self.ip: AnyIPAddress = None
        self.port: int = 0
        self.monitoring: MonitorConfig = None
        self.flavor: Dict[str, Any] = {}

    def serialize(self) -> Dict[str, Any]:
        return {
            'id': self.id,
            'notes': self.notes,
            'ip': str(self.ip),
            'port': self.port,
            'monitoring': self.monitoring.serialize(),
            'flavor': self.flavor,
        }

    def deserialize(self, data: Dict[str, Any]) -> None:
        self.id = data['id']
        self.notes = data['notes']
        self.ip = ip_address(data['ip'])
        self.port = int(data['port'])
        self.monitoring = MonitorConfig()
        self.monitoring.ip=self.ip
        self.monitoring.port=self.port
        self.monitoring.deserialize(data['monitoring'])
        self.flavor = data['flavor']

    def set_defaults(self) -> None:
        self.id = 'main'
        self.notes = 'FIXME'
        self.ip = IPv4Address('127.0.0.1')
        self.port = 7777
        self.flavor = {'id': 'ss13-vg'}
