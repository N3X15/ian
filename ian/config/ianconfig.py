from typing import Any, Dict

from ian.config.instanceconfig import InstanceConfig


class IanConfig:
    def __init__(self) -> None:
        self.instances: Dict[str, InstanceConfig] = {}

    def deserialize(self, data: Dict[str, Any]) -> None:
        self.instances = {}
        for k, v in data['instances'].items():
            i = InstanceConfig()
            i.set_defaults()
            i.deserialize(v)
            i.id = k
            self.instances[i.id] = i

    def serialize(self) -> Dict[str, Any]:
        o: Dict[str, Any] = {}
        o['instances'] = {i.id: i.serialize() for i in self.instances.values()}
        return o