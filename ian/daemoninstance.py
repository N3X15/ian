from pathlib import Path

import pygit2

from ian.config.instanceconfig import InstanceConfig
from ian.flavors.flavormanager import FlavorManager
from ian.flavors.iserverflavor import IServerFlavor
from ian.ipaddr import LOOPBACK, AnyIPAddress


class DaemonInstance:
    def __init__(self) -> None:
        #: Unique ID of this daemon instance
        self.id: str = ''
        #: Base path to the working directory
        self.basepath: Path = None
        #: IP address the daemon should bind to
        self.ip: AnyIPAddress = LOOPBACK
        #: Port the daemon should use
        self.port: int = 7777
        #: Game information
        self.flavor: IServerFlavor = None
        #: Actual monitoring setup
        self.monitoring: DaemonPortMonitor = None
        #: Git repo for game configuration
        self.config_repo: pygit2.Repository = None
        #: Git repo for game overwrites
        self.overwrite_repo: pygit2.Repository = None

    def configure(self, cfg: InstanceConfig) -> None:
        self.id = cfg.id
        self.basepath = cfg.basepath
        self.ip = cfg.ip
        self.port = cfg.port
        self.flavor = FlavorManager.getNewInstance(cfg.flavor)
        self.monitoring = DaemonPortMonitor()
        
    def setup(self) -> None:
        self.flavor.setup()