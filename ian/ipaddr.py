from ipaddress import IPv4Address, IPv6Address, ip_address
from typing import Union

AnyIPAddress = Union[IPv4Address, IPv6Address]

LOOPBACK: IPv4Address = ip_address('127.0.0.1')
BROADCAST: IPv4Address = ip_address('0.0.0.0')
