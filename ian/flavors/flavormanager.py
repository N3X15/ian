from typing import Any, Dict, Type
from ian.config.baseflavorconfig import BaseFlavorConfig

from ian.flavors.iserverflavor import IServerFlavor


class FlavorManager:
    FLAVORS: Dict[str, Type[IServerFlavor]] = {
        #VGStationFlavor.ID: VGStationFlavor,
    }

    @classmethod
    def getNewInstance(self, data: Dict[str, Any]) -> None:
        flavor = self.FLAVORS[data['id']]
        f = flavor()
        cfg = f.default_config()
        cfg.update(data)
        bfc: BaseFlavorConfig = f.CONFIG_TYPE()
        bfc.deserialize(data)
        f.configured(bfc)
        return f
        