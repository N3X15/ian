from abc import ABC, abstractmethod
from typing import Any, Dict, Type
from ian.config.baseflavorconfig import BaseFlavorConfig

from ian.eserverstatus import EServerStatus


class IServerFlavor(ABC):
    ID: str = ''
    CONFIG_TYPE: Type[BaseFlavorConfig] = BaseFlavorConfig

    def __init__(self) -> None:
        super().__init__()
        self.config: BaseFlavorConfig = None
        self.state: EServerStatus = EServerStatus.STOPPED

    @abstractmethod
    def default_config(self) -> Dict[str, Any]:
        pass

    #@abstractmethod
    def configured(self, data: Dict[str, Any]) -> None:
        self.config = self.CONFIG_TYPE()
        self.config.deserialize(data)

    @abstractmethod
    def start(self) -> None:
        pass

    @abstractmethod
    def stop(self) -> None:
        pass

    @abstractmethod
    def setup(self) -> None:
        pass

    @abstractmethod
    def clean(self) -> None:
        pass
