###
# This file compiles Nuitka binaries and i18n stuff.
###

import os
from pathlib import Path
from typing import List, Tuple
from buildtools.maestro import BuildMaestro
from buildtools import os_utils
from buildtools.maestro.nuitka import NuitkaTarget
from buildtools.maestro.fileio import CopyFileTarget
from buildtools.maestro.base_target import SingleBuildTarget
from babel.messages.frontend import CommandLineInterface as BabelCLI

AVAILABLE_LANGUAGES = frozenset(['en_US'])
LANGFILES: List[str] = []
LOCALE_DIR = Path('locale')

PYTHON = os_utils.assertWhich('python')

IAN_CONTRIBS: str = 'Ian Contributors'
IAN_FILE_VERSION: Tuple[int, int, int, int] = (1, 0, 0, 0)

class BabelCompileTarget(SingleBuildTarget):
    BT_LABEL = 'BABEL'

    def __init__(self, outputfile: Path, inputfile: Path, dependencies: List[str] = []):
        self.inputfile: Path = inputfile
        self.outputfile: Path = outputfile
        super().__init__(target=str(outputfile), files=[
            str(inputfile)], dependencies=dependencies)

    def build(self):
        BabelCLI().run(['pybabel', '-q', 'compile', '-i',
                     self.inputfile, '-o', self.outputfile])


def buildDomain(bm: BuildMaestro, domain: str) -> None:
    global LANGFILES, IAN_DIR
    for lang in AVAILABLE_LANGUAGES:
        outf = IAN_DIR / 'locale' / lang / 'LC_MESSAGES' / f'{domain}.mo'
        inf = LOCALE_DIR / lang / 'LC_MESSAGES' / f'{domain}.po'
        if not outf.parent.is_dir():
            outf.parent.mkdir(parents=True, exist_ok=True)
        LANGFILES.append(bm.add(BabelCompileTarget(outf, inf)).target)

DIST_DIR: Path = Path('dist')

IAN_DIR = Path('ian')
IAN_FILES: List[Path] = list(IAN_DIR.rglob('*.py'))

IAN_D_DIR = IAN_DIR / 'cli' / 'ian_d'
IAN_D_FILENAME = 'ian.d'
IAN_D_ENTRY_FILE = IAN_D_DIR / '__main__.py'
IAN_D_FILES: List[Path] = IAN_FILES + list(IAN_D_DIR.rglob('*.py'))
DIST_IAN_D = DIST_DIR / 'bin' / IAN_D_FILENAME

IAN_CLIENT_DIR = IAN_DIR / 'cli' / 'ian_d'
IAN_CLIENT_FILENAME = 'ian'
IAN_CLIENT_ENTRY_FILE = IAN_CLIENT_DIR / '__main__.py'
IAN_CLIENT_FILES: List[Path] = IAN_FILES + list(IAN_CLIENT_DIR.rglob('*.py'))
DIST_IAN_CLIENT = DIST_DIR / 'bin' / IAN_CLIENT_FILENAME

if os.name == 'nt':
    DIST_IAN_CLIENT = DIST_IAN_CLIENT.with_suffix('.exe')
    DIST_IAN_D      = DIST_IAN_D.with_suffix('.exe')
    
bm = BuildMaestro()

buildDomain(bm, 'ian.core')

# ian.d
iand_compile = bm.add(NuitkaTarget(str(IAN_D_ENTRY_FILE), '__main__', list(map(str, IAN_D_FILES)), single_file=True, nuitka_subdir='ian.d', dependencies=LANGFILES))
iand_compile.windows_product_name = 'Ian Daemon Monitor'
iand_compile.windows_company_name = IAN_CONTRIBS
iand_compile.windows_file_version = IAN_FILE_VERSION
iand_compile.windows_file_description = "Monitors and manages BYOND DreamDaemon instances automatically."
#iand_compile.enabled_plugins.add('anti-bloat')
iand_compile.enabled_plugins.add('pylint-warnings')
bm.add(CopyFileTarget(str(DIST_IAN_D), str(iand_compile.executable_mangled), dependencies=[iand_compile.target]))

# ian.client
ianclient_compile = bm.add(NuitkaTarget(str(IAN_CLIENT_ENTRY_FILE), '__main__', list(map(str, IAN_CLIENT_FILES)),
                           single_file=True, nuitka_subdir='ian.client', dependencies=LANGFILES))
ianclient_compile.windows_product_name = 'Ian Daemon Command Line Interface'
ianclient_compile.windows_company_name = IAN_CONTRIBS
ianclient_compile.windows_file_version = IAN_FILE_VERSION
ianclient_compile.windows_file_description = "Allows one to manage Ian's configuration and running instances."
#ianclient_compile.enabled_plugins.add('anti-bloat')
ianclient_compile.enabled_plugins.add('pylint-warnings')
bm.add(CopyFileTarget(str(DIST_IAN_CLIENT), str(ianclient_compile.executable_mangled), dependencies=[ianclient_compile.target]))

bm.as_app()