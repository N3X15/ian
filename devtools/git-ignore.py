'''
.gitignore Management THingy

LICENSE: MIT
'''

import argparse
import difflib
import os
from pathlib import Path
from typing import List, Optional, Sequence, Set


class GitIgnore:
    def __init__(self) -> None:
        self.lines: Set[str] = set()

    def add(self, line: str) -> None:
        self.lines.add(line.strip())

    def remove(self, line: str) -> None:
        self.lines.remove(line.strip())

    def load(self, filename: Path) -> None:
        self.lines = set()
        with filename.open('r') as f:
            for line in f:
                if line.strip().startswith('#'):
                    print(f'Comment skipped: {line!r}')
                    continue
                if line.strip() == '':
                    print(f'Empty line skipped')
                    continue
                self.add(line)

    def save(self, filename: Path) -> None:
        tmpfilename: Path = filename.with_name(filename.name+'~')
        with tmpfilename.open('w') as f:
            for line in sorted(self.lines):
                f.write(f'{line}\n')
        os.replace(tmpfilename, filename)

def addeol(s: str) -> str:
    return s + '\n'
def _register_cmd_add(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser('add')
    p.add_argument('entries', type=str, nargs='+')
    p.set_defaults(cmd=_cmd_add)
def _cmd_add(args: argparse.Namespace) -> None:
    g = GitIgnore()
    GIPATH = Path('.gitignore')
    g.load(GIPATH)
    oldentries: List[str] = list(map(addeol, sorted(g.lines)))
    e: str
    for e in args.entries:
        g.add(e)
    newentries: List[str] = list(map(addeol, sorted(g.lines)))
    print(''.join(difflib.unified_diff(oldentries, newentries)))
    g.save(GIPATH)
        
def _register_cmd_rm(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser('rm')
    p.add_argument('entries', type=str, nargs='+')
    p.set_defaults(cmd=_cmd_rm)
def _cmd_rm(args: argparse.Namespace) -> None:
    g = GitIgnore()
    GIPATH = Path('.gitignore')
    g.load(GIPATH)
    oldentries: List[str] = list(map(addeol, sorted(g.lines)))
    e: str
    for e in args.entries:
        g.remove(e)
    newentries: List[str] = list(map(addeol, sorted(g.lines)))
    print('\n'.join(difflib.unified_diff(oldentries, newentries))+'\n')
    g.save(GIPATH)
        
def _register_cmd_clean(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser('clean')
    p.set_defaults(cmd=_cmd_clean)
def _cmd_clean(args: argparse.Namespace) -> None:
    g = GitIgnore()
    GIPATH = Path('.gitignore')
    g.load(GIPATH)
    g.save(GIPATH)

def main(argv: Optional[Sequence[str]] = None) -> None:
    argp = argparse.ArgumentParser('git-ignore', description='.gitignore manager')
    
    subp = argp.add_subparsers()
    
    _register_cmd_add(subp)
    _register_cmd_clean(subp)
    _register_cmd_rm(subp)
    
    args = argp.parse_args(argv)

    if hasattr(args,'cmd'):
        args.cmd(args)
    else:
        argp.print_usage()

if __name__ == '__main__':
    main()
