# Ian
A modular and extensible DreamDaemon management system.

**CURRENTLY NOT WORKING, UNDER HEAVY DEVELOPMENT**

## License

See [the MIT LICENSE](LICENSE).

## Components

 * `ian[.client]` - CLI for Ian configuration and operations
 * `ian.d` - Actual background server manager/monitor.  Not touched by humans.

## Installation

Ian is compiled with [Nuitka](https://github.com/Nuitka/Nuitka) so it can be run without needing a runtime or dependencies.

### Windows (Source)

1. Install BYOND.
1. Install Python >=3.8 from https://python.org and ensure it's in `PATH`
1. Install [Poetry](https://python-poetry.org/docs/#installation).
1. Install a Git CLI (command line client) if it's not already present.
1. In cmd.exe, run: `git clone https://gitlab.com/N3X15/ian.git ian && cd ian && poetry install`

### Linux (Source)

1. Install git.
1. Install Python >=3.8, if it isn't already
1. Install pip for Python >=3.8
1. **Create a non-admin user for the gameserver**<br/>`sudo useradd --disabled-login --gecos byond`
1. Install [Poetry](https://python-poetry.org/docs/#installation) globally or as user.
1. Install BYOND for Linux *as the `byond` user*
1. Run `git clone https://gitlab.com/N3X15/ian.git ian && cd ian && poetry install` *as the `byond` user*

## Configuration

TODO
